#!/usr/bin/env python

# Copyright (c) 2007, 2008 Rocco Rutte <pdmef@gmx.net> and others.
# License: MIT <http://www.opensource.org/licenses/mit-license.php>

from mercurial import hg,ui
import re
import os
import sys
from subprocess import PIPE,Popen

# default git branch name
cfg_master='master'
# default origin name
origin_name=''
# default email address when unknown
unknown_addr='unknown'
split_name_re = re.compile(r'^((?:[^<]|<at>)*?)(?:<(?!at>)(?:(?:([^>]*)|(?:(.*)>(.*)))))?$', re.S|re.I)
git_crud = '\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f' \
           '\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f' \
           ' .,:;<>"\\' "'"
git_delch_re = re.compile(r'[<>\n]+', re.S)
spelled_at_re = re.compile(r'^(.*?)(?:@|(?:(?:^|\s)<at>(?:\s|$)))', re.I)

def set_default_branch(name):
  global cfg_master
  cfg_master = name

def set_origin_name(name):
  global origin_name
  origin_name = name

def setup_repo(url):
  try:
    myui=ui.ui(interactive=False)
  except TypeError:
    myui=ui.ui()
    myui.setconfig('ui', 'interactive', 'off')
  return myui,hg.repository(myui,url)

# Git strips "crud" characters off both the beginning and end of the user's name
# and the user's email, then deletes any remaining '<', '>' and '\n' characters
# before combining the user name with the user email surrounded by '<' and '>'.
# This function provides the crud-stripping and deletion operation that is used
# on both the name and email.
def gitname(name):
  name = name.strip(git_crud)
  return git_delch_re.sub('', name)

def set_unknown_addr(addr):
  global unknown_addr
  ans=False
  if addr!=None:
    addr=gitname(addr)
    if addr!='':
      unknown_addr=addr
      ans=True
  return ans

# Split the combined name and email input into a separate name and email and
# apply Git's rules to each part.  The idea is to use anything to the left of
# the first '<' and to the right of the last '>' as the name.  Anything between
# the first '<' and the last '>' is treated as the email.  If there is no '<'
# but the name contains '@' (which may be spelled out) then treat the entire
# thing as an email with no name.  If the detected name is empty then anything
# up to the first '@' (which may be spelled out) in the email is used for the
# name.  Failing that the entire email is used for the name.
def split_name_email(combined):
  name = ''
  email,rawemail = '',''
  match = split_name_re.match(combined)
  if match:
    left,rest,mid,right = match.groups()
    if rest != None:
      name = gitname(left)
      rawemail = rest
      email = gitname(rawemail)
    elif mid != None:
      name = gitname(left.rstrip() + ' ' + right.lstrip())
      rawemail = mid
      email = gitname(rawemail)
    else:
      name = gitname(left)
  if email == '' and spelled_at_re.match(left):
    rawemail = left
    email = name
    name = ''
  if name == '':
    at = spelled_at_re.match(rawemail)
    if at:
      name = gitname(at.group(1))
    if name == '':
      name = email
    # We do this test to be compatible with the previous behavior of hg2git.py
    # When it's given a <email> without any name and email does not contain '@'
    # then it sets the email to the unknown address
    if (len(left) < 2 or left[-1] != ' ') and not at:
      email = ''
  return [name, email]

def fixup_user(user,authors):
  user=user.strip("\"")
  if authors!=None:
    # if we have an authors table, try to get mapping
    # by defaulting to the current value of 'user'
    user=authors.get(user,user)
  name,mail=split_name_email(user)
  if mail == '':
    # If we don't have an email address replace it with unknown_addr.
    mail = unknown_addr
  if name == '':
    # Git does not like an empty name either -- split_name_email can only
    # return an empty name if it also returns an empty email.  This probably
    # will never happen since the input would have to be empty or only "crud"
    # characters, but check just to be safe.
    name = '-'
  return '%s <%s>' % (name,mail)

def get_branch(name):
  # 'HEAD' is the result of a bug in mutt's cvs->hg conversion,
  # other CVS imports may need it, too
  if name=='HEAD' or name=='default' or name=='':
    name=cfg_master
  if origin_name:
    return origin_name + '/' + name
  return name

def get_changeset(ui,repo,revision,authors={}):
  node=repo.lookup(revision)
  (manifest,user,(time,timezone),files,desc,extra)=repo.changelog.read(node)
  tz="%+03d%02d" % (-timezone / 3600, ((-timezone % 3600) / 60))
  branch=get_branch(extra.get('branch','master'))
  return (node,manifest,fixup_user(user,authors),(time,tz),files,desc,branch,extra)

def mangle_key(key):
  return key

def load_cache(filename,get_key=mangle_key):
  cache={}
  if not os.path.exists(filename):
    return cache
  f=open(filename,'r')
  l=0
  for line in f.readlines():
    l+=1
    fields=line.split(' ')
    if fields==None or not len(fields)==2 or fields[0][0]!=':':
      sys.stderr.write('Invalid file format in [%s], line %d\n' % (filename,l))
      continue
    # put key:value in cache, key without ^:
    cache[get_key(fields[0][1:])]=fields[1].split('\n')[0]
  f.close()
  return cache

def save_cache(filename,cache):
  f=open(filename,'w+')
  map(lambda x: f.write(':%s %s\n' % (str(x),str(cache.get(x)))),cache.keys())
  f.close()

def get_git_sha1(name,type='heads'):
  try:
    # use git-rev-parse to support packed refs
    ref="refs/%s/%s" % (type,name)
    proc = Popen(["git", "rev-parse", "--verify", "--quiet", ref], stdout=PIPE)
    l = proc.communicate()[0]
    if l == None or len(l) == 0:
      return None
    return l[0:40]
  except:
    return None
